pipeline {
    agent any
    tools {
      maven "Maven"
      jdk "openjdk-17"
    }
    stages {
      stage('compile') {
        steps {
          sh ("""
            java -version
            chmod +x mvnw 
            ./mvnw clean 
          """)
          archiveArtifacts artifacts: 'target/*.jar', onlyIfSuccessful: true
        }
      }
      stage('Unit Test') {
        steps {
          sh './mvnw test  '
        }
      }
    }
}